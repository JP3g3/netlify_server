# Simple nodejs backend for [my_website](https://bitbucket.org/JP3g3/my_website)

## POST method /mail
#### required body:

```json 
{
    "mail": "mail@mail.com",
    "subject": "subject",
    "text": "text" 
}
```

#### Response:

* if ok:

    status code = 200
```json
    { "message": "The mail has been sent!" }
```

* else:

    status code = 500
```json
    { "message": error}
```