require('dotenv').config()

const nodemailer = require("nodemailer")

const sendMail = async (mail, subject, text) => {

  const msg = {
    to: mail,
    from: process.env.USER,
    subject,
    text
  }

  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      user: process.env.USER,
      pass: process.env.PASS
    }
  })

  await transporter.sendMail(msg)
}

module.exports = sendMail