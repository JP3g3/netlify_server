const express = require('express')
const cors = require('cors')
const serverless = require('serverless-http')
const sendMail = require('./mail')

const app = express()
const router = express.Router()

router.get('/', (req, res) => {
  res.send("Server is ready!")
})

router.post('/mail', cors(), (req, res) => {
  const { mail, subject, text } = req.body

  if (mail && subject && text) {
    sendMail(mail, subject, text)
      .then(() => {
        res.status(200).json({ message: "The mail has been sent!" })
      })
      .catch((err) => {
        console.log(error)
        res.status(500).json({ message: err })
      })
  } else {
    res.status(400).json({ message: "Bad request" })
  }
})

app.use(cors())
app.use(express.json())
app.use('/.netlify/functions/server', router)

module.exports = app
module.exports.handler = serverless(app)
